// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_EnviromentStructure.h"
#include "Materials/MaterialInterface.h"
#include "Physicalmaterials/PhysicalMaterial.h"

// Sets default values
ATPS_EnviromentStructure::ATPS_EnviromentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATPS_EnviromentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATPS_EnviromentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface ATPS_EnviromentStructure::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}
TArray<UTPS_StateEffects*> ATPS_EnviromentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void ATPS_EnviromentStructure::RemoveEffect(UTPS_StateEffects* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATPS_EnviromentStructure::AddEffect(UTPS_StateEffects* newEffect)
{
	Effects.Add(newEffect);
}

